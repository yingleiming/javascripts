有三种方法可以提取字符串的一部分。

<font color="blue">01、Substr</font>

该 **substr(start, length)** 方法提取字符串的一部分，从指定的索引处开始，并返回指定数量的字符。

```javascript
const quote = "Winter is coming"; 
const part1 = quote.substr(0, 6); 
console.log(part1);
//Winter 
const part2 = quote.substr(10, 6);
console.log(part2); 
//coming 
```

**<font color="red">请注意，第一个字符在 index 处为0。</font>**

该 **start** 参数是必须的，但是 **length** 是可选的。如果省略，它将提取字符串的其余部分。

```js
const quote = "Winter is coming"; 
const part = quote.substr(6); 
console.log(part); 
// is coming
```

<font color="blue">02、Substring</font>

该 **substring(start, end)** 方法返回 **start** 和 **end** 索引之间的字符串部分。它从 **start** 索引处的字符开始到结束，**<font color="red">但不包括索引处的字符end</font>**。

```js
const quote = "We Stand Together";
const part = quote.substring(3, 7);
console.log(part);
//Stan
```

如果 **end** 省略索引，它将提取到字符串的末尾。

```js
const quote = "We Stand Together";
const part = quote.substring(3);
console.log(part);
//Stand Together
```

与 **indexOf** 方法结合使用，效果会更好 。

该 **indexOf** 方法返回第一个索引，在该索引处可以找到给定的字符串文本，否则返回 **-1**。

```js
const quote = "You know nothing, Json Snow";
const commaIndex = quote.indexOf(",");
const part = quote.substring(commaIndex + 1);
console.log(part);
// Json Snow
```

<font color="blue">03、Slice</font>

该 **slice(start, end)** 方法返回 **start** 和 **end** 索引之间的字符串部分。**slice** 像 **substring**。

```js
const quote = "We Stand Together";
const part = quote.slice(3, 7);
console.log(part);
//Stan
```

如果 **end** 省略索引，它将提取到字符串的末尾。

```js
const quote = "We Stand Together";
const part = quote.slice(3);
console.log(part);
//Stand Together
```



**<font color="red">字符串在 JavaScript 中是不可变的。所以这些方法都不会更改原始字符串。</font>**



