// 获取正则的修饰符
function getFlags (re) {
  var text = re.toString();
  return text.substring(text.lastIndexOf("/") + 1, text.length);
}

var reg = /ab/g;
console.log(getFlags(reg));// "g"