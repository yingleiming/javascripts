var fruits = ["Banana", "Orange", "Apple", "Mango"];
var energy = fruits.join("，");
console.log(energy); //Banana，Orange，Apple，Mango
console.log(fruits);



var numbers = ["10", "20", "30", "40"];
var s_num = numbers.join("<");
console.log(s_num); //10<20<30<40
console.log(numbers); //["10", "20", "30", "40"]


var fruits = ["Banana", "Orange", "Apple", "Mango"];
var energy = fruits.join(" and ");
console.log(energy); //Banana and Orange and Apple and Mango
console.log(fruits); //["Banana", "Orange", "Apple", "Mango"]

function repeatStr(str, n) {
    return new Array(n + 1).join(str);
}

console.log(repeatStr("a", 5));

var fruits = ["Banana", "Orange", "Apple", "Mango"];
console.log(fruits.sort()); //["Apple", "Banana", "Mango", "Orange"]
console.log(fruits); //["Apple", "Banana", "Mango", "Orange"]

var points = [-40, 100, 1, 5, -25, 10];
console.log(points.sort((a, b) => {
    return a - b;
}));


var fruits = ["Banana", "Orange", "Apple", "Mango"];
console.log(fruits.reverse()); //["Mango", "Apple", "Orange", "Banana"]
console.log(fruits); //["Mango", "Apple", "Orange", "Banana"]

var num_1 = ["10", "20"];
var num_2 = ["30", "40",["50","60",["70","80"]]];
console.log(num_1.concat(num_2)); 
console.log(num_1, num_2); //(2) ["10", "20"] (2) ["30", "40"]