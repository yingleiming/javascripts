1. js 的数据类型有哪些？
   基本数据类型(简单数据类型)：String、 Number、 Boolean、 Null、 Undefined
   引用数据类型(也就是对象数据类型是 object)：Object、Array、Function、Date
   ES6 新类型：symbol
2. 检测数据类型的方法有哪些？
   typeOf、instanceOf、constructor、Object.prototype.toString.call()
3. 基本数据类型和引用数据类型的区别是什么？
   1）基本数据类型存储在栈中，引用数据类型存储在堆中
   2）访问机制不同
   3）复制变量时的不同
   4）参数传递的不同
4. [提取字符串的方法有哪些？](https://blog.csdn.net/yingleiming/article/details/117806512)
   (1)substr、(2)substring、(3)slice
5. 实现继承的方式有哪些？
   (1)[原型链继承](https://blog.csdn.net/yingleiming/article/details/105688423)、(2)[借用构造函数继承](https://blog.csdn.net/yingleiming/article/details/105688546)、(3)[组合继承](https://blog.csdn.net/yingleiming/article/details/105688572)、(4)[原型式继承](https://blog.csdn.net/yingleiming/article/details/105688597)、(5)[寄生式继承](https://blog.csdn.net/yingleiming/article/details/105688608)、(6)[寄生组合式继承](https://blog.csdn.net/yingleiming/article/details/105688630)
6. [创建对象的五种常见方法？](https://blog.csdn.net/yingleiming/article/details/117825043)
   (1)字面量、(2)工厂模式、(3)构造函数、(4)原型模式、(5)构造函数与原型混合(最优)
7. 创建数组的两种方式？
   (1)字面量、(2)使用 Array 构造函数
8. [处理数组的常用方法有哪些？](https://www.cnblogs.com/songyao666/p/11107237.html)
   join()、push()和 pop()、shift()和 unshift()、sort()、reverse()、concat()、slice()与 splice()与 split()、indexOf()和 lastIndexOf()、forEach()、map()、filter()、every()、some()、reduce()和 reduceRight()
9. [js 的哪些操作会造成内存泄漏？](https://www.jianshu.com/p/763ba9562864)
   内存泄漏是指一块被分配的内存既不能使用，也不能被回收，直到浏览器进程结束。
   (1)、意外的全局变量；(2)、闭包；(3)、没有清理的 DOM 元素，dom 元素赋值给变量，又通过 removeChild 移除。但是 dom 元素的引用还在内存中；(4)、被遗忘的定时器或者回调
10. [创建函数的两种方式？](https://blog.csdn.net/yingleiming/article/details/118145323)
    (1)函数声明、(2)函数表达式
11. [哪些情况会产生跨域问题及解决办法？](https://segmentfault.com/a/1190000011145364)
    协议不同，域名不同，端口不同均会产生跨域问题；解决办法：JSONP，CROS，使用 http-proxy-middleware 中间件...
12. 前端错误类型有哪些？如何优雅的处理异常呢？
    [SyntaxError(语法错误)](https://editor.csdn.net/md/?articleId=118186977)、[ReferenceError(引用错误)](https://editor.csdn.net/md/?articleId=118186995)、[TypeError(类型错误)](https://editor.csdn.net/md/?articleId=118187009)、[RangeError(范围错误)](https://editor.csdn.net/md/?articleId=118187028)、[URIError(URL 错误)](https://editor.csdn.net/md/?articleId=118187046)
13. 删除数组中的某项元素？

14. [介绍一下暂时性死区？](https://www.gxlcms.com/JavaScript-253414.html)
    在代码块内，使用 let、const 命令声明变量之前，该变量都是不可用的。这在语法上，成为"暂时性死区"（temporal dead zone,简称 TDZ）

15. 浅谈函数的防抖和节流？
    函数节流(throttle)和函数防抖(debounce)都是为了限制函数的执行频次，以优化函数触发频率过高导致的响应速度跟不上触发频率，出现延迟、假死或卡顿的现象。
