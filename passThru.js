//模拟模板字面量的默认行为
function passThru(literals, ...substitutions) {
  let result = ''
  //根据substitutions的数量来确定循环的执行次数
  for (let i = 0; i < substitutions.length; i++) {
    result += literals[i]
    result += substitutions[i]
  }
  //合并最后一个literals
  result += literals[substitutions.length - 1]
  return result
}

let count = 10,
  price = 0.25,
  message = passThru`${count} items cost $${(count * price).toFixed(2)}`
console.log(message)

//“标签模板”的一个重要应用，就是过滤 HTML 字符串，防止用户输入恶意内容。
