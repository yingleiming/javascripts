**SyntaxError**:**语法错误**。

总体上产生这种错误的原因多是因为 js 代码在书写时不规范造成的，例如少了一个分号、逗号，或者双引号里面嵌套了双引号等等容易忽视的错误造成的。

1.<font color=red>Uncaught SyntaxError: Invalid or unexpected token</font>：无效或者意外的标记

```js
 for(var i=0;i<data.length;i++){
        content+="<li><a href="#">"+data[i].cname+"</a></li>"
	alert(data[i]);
 }
```

错误原因：因为外部已经使用了双引号，里面必须使用单引号

2.<font color=red>Uncaught SyntaxError: Unexpected token ')'</font>

```js
javascript:void();//错误写法
<a href="javascript:void(0);"></a>//或者
<a href="javascript:;"></a>
```

错误原因：href的属性 "javascript:void()",括号中没有加"0"，表达式"0"必不可少。

3.<font color=red>SyntaxError:Unexpected identifier</font>:不期望的标识符

```js
$("#test").html("<input type='button' value='按钮' onclick='alert(\'123a\')'/>");//错误的引号嵌套
$("#test").html("<input type='button' value='按钮' onclick='alert(\"123a\")'/>");//正确的引号嵌套
```

一般是代码书写不规范，如缺少逗号，或符号使用错误

4.<font color=red>Uncaught SyntaxError: Unexpected identifier</font>

```js
var person = {
    name: "李白" //缺少逗号
    age: 300
}
console.log(person);
```

错误原因：name属性后缺少逗号

5.<font color=red>Uncaught SyntaxError: Invalid or unexpected token</font>

```js
var 1a;//错误的变量命名
```

错误原因：变量命名错误导致；变量命名规范：1.不能以数字开头2.不能是关键字或保留字3.区分大小写

6.<font color=red>Uncaught SyntaxError: missing ) after argument list</font>

```js
console.log("missing"
```

错误原因：缺少右括号 ")"